import { Action } from '@ngrx/store';
import { IUser } from '../modules/employee/list-employee.component';

export enum AppActionTypes {
  SetEmployee = '[App] Set Employee',
  SetEmployees = '[App] Set Employees',
}

export class SetEmployee implements Action {
  readonly type = AppActionTypes.SetEmployee;

  constructor(public payload: IUser) {}
}

export class SetEmployees implements Action {
    readonly type = AppActionTypes.SetEmployees;
  
    constructor(public payload: IUser[]) {}
  }

export type AppActions = SetEmployee | SetEmployees;