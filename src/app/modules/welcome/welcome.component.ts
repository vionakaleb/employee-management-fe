import { Component, OnInit } from '@angular/core';
import { mockUserData } from 'src/app/utils/mock-user-data';
import { IUser } from '../employee/list-employee.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  listOfDisplayData: IUser[] = mockUserData;

  constructor() { }

  ngOnInit() {
  }

}
