import { NgModule } from '@angular/core';

import { EmployeeRoutingModule } from './employee-routing.module';
import { FormEmployeeComponent } from './form-employee.component';
import { ListEmployeeComponent } from './list-employee.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [EmployeeRoutingModule, NgZorroAntdModule, FormsModule, ReactiveFormsModule, CommonModule],
  declarations: [FormEmployeeComponent, ListEmployeeComponent],
  exports: [FormEmployeeComponent, ListEmployeeComponent]
})
export class EmployeeModule { }