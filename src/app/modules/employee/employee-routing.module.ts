import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormEmployeeComponent } from './form-employee.component';
import { ListEmployeeComponent } from './list-employee.component';

const routes: Routes = [
    {
      path: '',
      pathMatch: 'full', 
      redirectTo: '/dashboard/employee/create'
    },
    { path: 'list', component: ListEmployeeComponent },
    { path: 'create', component: FormEmployeeComponent },
    { path: 'edit/:id', component: FormEmployeeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }