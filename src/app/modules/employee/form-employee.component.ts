import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'form-employee',
    templateUrl: 'form-employee.component.html',
    styleUrls: ['form-employee.component.scss']
})

export class FormEmployeeComponent implements OnInit {
    constructor(private fb: FormBuilder, private router: Router) {}

    isEdit = this.router.url.includes('edit');
    localData = JSON.parse(localStorage.getItem('form-employee')) || {};

    ngOnInit(): void {
      if (this.isEdit && this.localData.id) {
        this.validateForm = this.fb.group({
          id: this.localData.id,
          username: this.localData.username,
          firstName: this.localData.firstName,
          lastName: this.localData.lastName,
          email: this.localData.email,
          birthDate: this.localData.birthDate,
          basicSalary: this.localData.basicSalary,
          status: this.localData.status,
          group: this.localData.group,
          description: this.localData.description,
        });
      } else {
        this.validateForm = this.fb.group({
          id: +Date.now(),
          username: [null, [Validators.required]],
          firstName: [null, [Validators.required]],
          lastName: [null, [Validators.required]],
          email: [null, [Validators.email, Validators.required]],
          birthDate: [null, [Validators.required]],
          basicSalary: [null, [Validators.required]],
          status: [null, [Validators.required]],
          group: [null, [Validators.required]],
          description: [null, [Validators.required]],
        });
      }
    }
    
    validateForm: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    localStorage.setItem('form-employee', JSON.stringify(this.validateForm.value))
    this.router.navigate(['/dashboard/employee/list'])
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };
}