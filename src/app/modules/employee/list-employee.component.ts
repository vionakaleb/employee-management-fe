import { Component, OnInit } from '@angular/core';
import { mockUserData } from '../../utils/mock-user-data';
import { formatCurrencyVal } from 'src/app/utils/helpers';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromEmployees from '../../reducers/employees.reducer';
import * as fromActions from '../../actions/employees.actions';

export interface IUser {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

@Component({
    selector: 'list-employee',
    templateUrl: 'list-employee.component.html',
    styleUrls: ['list-employee.component.scss']
})

export class ListEmployeeComponent {
  constructor(private router: Router, private store: Store<fromEmployees.State>) {}

  searchValue = '';
  isAscending = true;
  listOfData: IUser[] = [...mockUserData];
  listOfDisplayData = [...this.listOfData];

  localData = JSON.parse(localStorage.getItem('form-employee')) || {};

  ngOnInit(): void {
    const store = this.store.select(fromEmployees.selectEmployees);
    console.log(store, "store")
    console.log(this.localData, "localData")
    console.log(this.listOfDisplayData, "listOfDisplayData")

    const isExist = this.listOfData.some(data => data.id === this.localData.id);

    // Check if item.id from local storage exist in list, then replace it (Update Process)
    if (isExist && this.localData.id) {
      this.replaceDataById();
    }

    // Check if there is item in local storage, then add to array (Create Process)
    if (!isExist && this.localData.id) {
      this.listOfData.unshift(this.localData)
      this.listOfDisplayData.unshift(this.localData)
    }
  }

  listOfColumn = [
    {
      title: 'Username',
      asc: (a: IUser, b: IUser) => a.username.localeCompare(b.username),
      desc: (a: IUser, b: IUser) => b.username.localeCompare(a.username),
    },
    {
      title: 'First Name',
      asc: (a: IUser, b: IUser) => a.firstName.localeCompare(b.firstName),
      desc: (a: IUser, b: IUser) => b.firstName.localeCompare(a.firstName),
    },
    {
      title: 'Last Name',
      asc: (a: IUser, b: IUser) => a.lastName.localeCompare(b.lastName),
      desc: (a: IUser, b: IUser) => b.lastName.localeCompare(a.lastName),
    },
    {
      title: 'Email',
      asc: (a: IUser, b: IUser) => a.email.localeCompare(b.email),
      desc: (a: IUser, b: IUser) => b.email.localeCompare(a.email),
    },
    {
      title: 'Birth Date',
      asc: (a: IUser, b: IUser) => a.birthDate.localeCompare(b.birthDate),
      desc: (a: IUser, b: IUser) => b.birthDate.localeCompare(a.birthDate),
    },
    {
      title: 'Basic Salary',
      asc: (a: IUser, b: IUser) => a.basicSalary - b.basicSalary,
      desc: (a: IUser, b: IUser) => b.basicSalary - a.basicSalary,
    },
    {
      title: 'Status',
      asc: (a: IUser, b: IUser) => a.status.localeCompare(b.status),
      desc: (a: IUser, b: IUser) => b.status.localeCompare(a.status),
    },
    {
      title: 'Group',
      asc: (a: IUser, b: IUser) => a.group.localeCompare(b.group),
      desc: (a: IUser, b: IUser) => b.group.localeCompare(a.group),
    },
    {
      title: 'Description',
      asc: (a: IUser, b: IUser) => a.description.localeCompare(b.description),
      desc: (a: IUser, b: IUser) => b.description.localeCompare(a.description),
    },
    {
      title: 'Action',
    }
  ];

  replaceDataById(): void {
    const localDataId = this.localData.id;

    // Find the index of the data with the same ID in listOfData
    const dataIndex = this.listOfData.findIndex(data => data.id === localDataId);

    if (dataIndex !== -1) {
      // Replace the data at the found index with localData
      this.listOfData[dataIndex] = { ...this.localData };
      this.listOfDisplayData[dataIndex] = { ...this.localData };
    }
  }

  search(): void {
    this.listOfDisplayData = this.listOfData.filter((item: IUser) => {
      const attribute =
        item.username.toLowerCase()
        || item.firstName.toLowerCase()
        || item.lastName.toLowerCase();

      return attribute.includes(this.searchValue);
    });
  }

  sort(title): void {
    this.isAscending = !this.isAscending;
    
    this.listOfColumn.forEach(item => {
      if (item.title === title) {
        this.listOfDisplayData = this.isAscending 
          ? this.listOfDisplayData.sort(item.asc) 
          : this.listOfDisplayData.sort(item.desc)
      };
    });
  }

  reset(): void {
    this.searchValue = '';
    this.search();
    this.listOfDisplayData = this.listOfData;
  }

  create(id: number): void {
    this.router.navigate(['/dashboard/employee/create'])
  }

  editRow(data: IUser): void {
    localStorage.setItem('form-employee', JSON.stringify(data))
    this.router.navigate([`/dashboard/employee/edit/` + data.id])
  }

  deleteRow(id: number): void {
    this.listOfDisplayData = this.listOfDisplayData.filter(d => d.id !== id);
  }

  formatCurrency(number: number) {
    return formatCurrencyVal(number)
  }
}
