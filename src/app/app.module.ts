import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsProviderModule } from './icons-provider.module';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { LoginComponent } from './login/login.component';
import { SharedModule } from './shared/shared.module';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { CoreModule } from './core/core.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { reducers } from './reducers';
import {storageMetaReducer} from './storage.metareducer';

import {StoreModule, MetaReducer, META_REDUCERS} from '@ngrx/store';
import {ROOT_STORAGE_KEYS, ROOT_LOCAL_STORAGE_KEY} from './app.tokens';
import {LocalStorageService} from './local-storage.service';

registerLocaleData(en);

// factory meta-reducer configuration function
export function getMetaReducers(saveKeys: string[],
  localStorageKey: string,
  storageService: LocalStorageService
 ): MetaReducer<any>[] {
  return [storageMetaReducer(saveKeys, localStorageKey, storageService)];
}

@NgModule({
  declarations: [AppComponent, LoginComponent, DashboardComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsProviderModule,
    NzIconModule,
    NgZorroAntdModule,
    NzFormModule,
    NzInputModule,
    NzAvatarModule,
    NzCardModule,
    NzTabsModule,
    NzGridModule,
    CoreModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    // { provide: ROOT_STORAGE_KEYS, useValue: ['layout.theme'] },
    // { provide: ROOT_LOCAL_STORAGE_KEY, useValue: '__app_storage__' },
    // {
    //   provide   : META_REDUCERS,
    //   deps      : [ROOT_STORAGE_KEYS, ROOT_LOCAL_STORAGE_KEY, LocalStorageService],
    //   useFactory: getMetaReducers
    // },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
