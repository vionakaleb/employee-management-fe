import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromActions from '../actions/employees.actions';
import { mockUserData } from '../utils/mock-user-data';
import { IUser } from '../modules/employee/list-employee.component';


export interface State {
    employee: IUser;
    employees: IUser[];
}

const initialState: State = {
  employee: {
		id: 1,
		username: "Karina",
		firstName: "Allistair",
		lastName: "Amity",
		email: "nulla@outlook.couk",
		birthDate: "2024-08-17 21:21:33",
		basicSalary: 16376867,
		status: "active",
		group: "bod",
		description: "2023-05-23 11:45:29"
	},
  employees: mockUserData
};

export const state = createFeatureSelector('employees');
export const selectEmployee = createSelector(state, (employeeState: State): IUser => employeeState.employee);
export const selectEmployees = createSelector(state, (employeesState: State): IUser[] => employeesState.employees);

export function reducer(state = initialState, action: fromActions.AppActions): State {
  switch (action.type) {
    case fromActions.AppActionTypes.SetEmployee:
        return {...state, employee: action.payload};
    case fromActions.AppActionTypes.SetEmployees:
      return {...state, employees: state.employees};
    default:
      return state;
  }
}