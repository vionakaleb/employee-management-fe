import {ActionReducerMap, createSelector} from '@ngrx/store';
import * as fromEmployees from './employees.reducer';
import { IUser } from '../modules/employee/list-employee.component';

export interface State {
  employee: fromEmployees.State;
  employees: fromEmployees.State;
}

export const reducers: ActionReducerMap<State> = {
  employee: fromEmployees.reducer,
  employees: fromEmployees.reducer
};

export const layout = ((state): fromEmployees.State => state.layout);
export const getActiveEmployee = createSelector(layout, (state): IUser => state.employee);