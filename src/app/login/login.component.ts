import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../core/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private fb: FormBuilder, private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  loginForm: FormGroup;

  submit() {
    const { 
      username: { value: username },
      password: { value: password },
    } = this.loginForm.controls
    
    let data = {
      username,
      password
    };

    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty();
      this.loginForm.controls[i].updateValueAndValidity();
    }

    if (!data.username || !data.password) {
      alert('Please fill in username and password')
    } else {
      this.userService.login(data).subscribe(response => {
        if (response && response.token) {
          localStorage.setItem('token', response.token.accessToken);
          let date = new Date();
          let hours = Math.round(response.token.expiresIn / 3600);
          date.setHours(date.getHours() + hours);
  
          localStorage.setItem('expires', JSON.stringify(date));
  
          // Redirect the user to home
          this.router.navigate(['/dashboard'])
        }
      });
    }   
  }
}
