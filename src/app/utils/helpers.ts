export function formatCurrencyVal(number: number) {
    return new Intl.NumberFormat('id-ID', {
      currency: 'IDR',
      minimumFractionDigits: 0,
      style: 'currency',
    }).format(number || 0);
}
