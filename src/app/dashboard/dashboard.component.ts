import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isCollapsed = false;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {}

  back(): void {
    this.router.navigate(["/dashboard/welcome"]);
}

  logout() {
    this.userService.logout();
  }
}
