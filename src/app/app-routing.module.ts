import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './core/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: 'login', component: LoginComponent },
  { 
    path: 'dashboard', 
    component: DashboardComponent, 
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        pathMatch: 'full', 
        redirectTo: '/dashboard/welcome',
        canActivate: [AuthGuard]
      },
      {
        path: 'welcome',
        loadChildren: () =>
          import('./modules/welcome/welcome.module').then(m => m.WelcomeModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'employee',
        loadChildren: () =>
          import('./modules/employee/employee.module').then(m => m.EmployeeModule),
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
